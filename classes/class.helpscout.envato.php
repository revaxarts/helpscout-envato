<?php

include 'classes/HelpScout/ApiClient.php';

use \HelpScout\ApiClient;

class helpscout_envato{

    private $debug = false;
    private $messagesubject = "] Message sent via your Envato Market profile from ";
    private $commentsubject = "] New comment on ";
    private $keep_messages = '1 day';

    private $loggedin = NULL;
    private $envato_scrapper = NULL;
    private $comment_signature;
    private $ignore_comments_from_users = array();


    public function __construct(){


        $this->envato_username = ENVATO_USERNAME;
        $this->envato_password  = ENVATO_PASSWORD;

        $this->notification_mail = NOTIFICAITON_MAIL;

        $this->server  = ENVATO_IMAP_SERVER;
        $this->port  = ENVATO_IMAP_PORT;
        $this->user  = ENVATO_IMAP_USER;
        $this->password  = ENVATO_IMAP_PASSWORD;
        $this->ssl  = ENVATO_IMAP_SSL;

        $this->comment_server = defined('ENVATO_COMMENT_IMAP_SERVER') ? ENVATO_COMMENT_IMAP_SERVER : $this->server;
        $this->comment_port = defined('ENVATO_COMMENT_IMAP_PORT') ? ENVATO_COMMENT_IMAP_PORT : $this->port;
        $this->comment_user  = defined('ENVATO_COMMENT_IMAP_USER') ? ENVATO_COMMENT_IMAP_USER : $this->user;
        $this->comment_password  = defined('ENVATO_COMMENT_IMAP_PASSWORD') ? ENVATO_COMMENT_IMAP_PASSWORD : $this->password;
        $this->comment_ssl  = defined('ENVATO_COMMENT_IMAP_SSL') ? ENVATO_COMMENT_IMAP_SSL : $this->ssl;

        $this->catchall_server  = ENVATO_CATCH_ALL_IMAP_SERVER;
        $this->catchall_port  = ENVATO_CATCH_ALL_IMAP_PORT;
        $this->catchall_user  = ENVATO_CATCH_ALL_IMAP_USER;
        $this->catchall_password  = ENVATO_CATCH_ALL_IMAP_PASSWORD;
        $this->catchall_ssl  = ENVATO_CATCH_ALL_IMAP_SSL;

        $this->h_apikey  = HELPSCOUT_APIKEY;
        $this->h_inbox  = HELPSCOUT_ENVATO_INBOX;
        $this->h_comment_inbox  = HELPSCOUT_ENVATO_COMMENT_INBOX;
        $this->h_html  = HELPSCOUT_HTML_MAIL;
       
        $this->imgur_api_id = IMGUR_API_ID;

        $this->ignore_comments($this->envato_username);

    }

    public function debug($bool = true){

        $this->debug = !!$bool;

    }

    public function comment_signature($html = ''){
        $this->comment_signature = $html;
    }

    public function keep_messages($keep = ''){
        $this->keep_messages = $keep;
    }

    public function ignore_comments($usernames){
        if(is_string($usernames)) $usernames = array($usernames);

        $this->ignore_comments_from_users = array_unique(array_merge($this->ignore_comments_from_users, $usernames));
    }

    public function check_inbox(){

        $hostname = "{" . $this->server . ":" .$this->port . "/imap".($this->ssl ? "/ssl" : '')."/novalidate-cert}";

        $mbox = @imap_open($hostname."INBOX", $this->user, $this->password);

        if(!$mbox) die("Couldn't connect to host!"); 

        $this->log('Check for Messages');

        $search = 'SUBJECT "'.$this->messagesubject.'" SINCE "'.date('j F Y', strtotime('- 3 day')).'" TO "'.$this->user.'" FROM "do-not-reply@market.envato.com" UNFLAGGED';
        $search = 'SUBJECT "'.$this->messagesubject.'" SINCE "'.date('j F Y', strtotime('- 3 day')).'" TO "'.$this->user.'" UNFLAGGED';

        $m = imap_search($mbox, $search);
        
        $count = $m ? count($m) : 0;
        
        $this->log($count.' messages found!');


        if(!$count) return;

        foreach($m as $mailid){

            $mail = $this->get_message($mbox, $mailid);

            $flag = false;

            //get the original message 
            $message = trim(substr($mail->message, 0, strrpos($mail->message, '---------')));
          
            if($this->h_html){
                $message = htmlentities(utf8_encode($message));
               $message = nl2br($message);
           }

//die();
            // get marketplace username
            preg_match('#This email was sent from (.*) <(.*)>#', $mail->message, $hits);
            $username = trim(strip_tags($hits[1]));

            //get purchases link
            preg_match('#http://.*?\?inspection_key=.*#', $mail->message, $hits);

            $purchaselink = trim($hits[0]);

            $this->log($username.' sent a message:');
            echo '<textarea style="width:400px;height:200px;">'.print_r($message, true).'</textarea>';

            //is not your own messsage
            if($username != $this->envato_username){
                $subject = "Message sent via your profile from $username on ".date('Y-m-d', $mail->timestamp);
                $note = $this->h_html ? '<a href="'.$purchaselink.'">Purchaselink</a>' : $purchaselink;
            }else{
                $username = $message->sender;
                $subject = "RE: Message sent via your profile from $username on ".date('Y-m-d', $mail->timestamp);
                $note = '';
            }

           // continue;

            //send to helpscout
            $flag = $this->to_helpscout($this->h_inbox, $username, $mail->reply_to, $message, $subject, $note);
            
            //flag message
            if($flag) imap_setflag_full($mbox, $mailid, "\\Flagged");

        }

         //die();
        imap_expunge($mbox);
        imap_close($mbox);

    }


    public function check_comments(){

        $hostname = "{" . $this->comment_server . ":" .$this->comment_port . "/imap".($this->comment_ssl ? "/ssl" : '')."/novalidate-cert}";

        $mbox = @imap_open($hostname."INBOX", $this->comment_user, $this->comment_password);

        if(!$mbox) die("Couldn't connect to host!"); 

        $this->log('Check for Comments');

        $search = 'SUBJECT "'.$this->commentsubject.'" SINCE "'.date('j F Y', strtotime('- 3 day')).'" TO "'.$this->comment_user.'" FROM "do-not-reply@market.envato.com" UNFLAGGED';

        $m = imap_search($mbox, $search);
        
        $count = $m ? count($m) : 0;
        
        $this->log($count.' comments found!');

        if(!$count) return;

        foreach($m as $mailid){

            $mail = $this->get_message($mbox, $mailid);

            $flag = false;

            //get message without the "You can reply to the comment here..."
            preg_match('@You can reply to the comment here: ((.*?)\/(\d+)\/comments(.*?)(.*comment_(\d+)))@', $mail->org_message, $hits);
            $message = $mail->org_message;
            if(empty($hits)){
                preg_match('@You can reply to the comment here: ((.*?)\/(\d+)\/comments(.*?)(.*comment_(\d+)))@', $mail->message, $hits);
                $message = $mail->message;

            }

            //get marketplace
            $marketplace = strstr($hits[2], '.net/', true).'.net';

            $message = (html_entity_decode(trim(substr($message,0, strpos($message, $hits[0])))));

            // get comment ID
            $comment_id = intval(trim($hits[6]));
            $item_id = intval(trim($hits[3]));
            $replylink = trim($hits[1]);

            //check if it is a reply (we need original comment ID)
            $base_comment_id = $this->get_base_comment_id($replylink, $comment_id);

            //for some reason we one of them is missing
            if(!$base_comment_id || !$item_id){

                $error = "Not able to get IDS:\n\n=========\n\n{$message}\n=========\n\nComment ID: {$base_comment_id}\nItem ID: {$item_id}";
                $this->error(__FUNCTION__, $error);
                $this->log('Not able to get IDS');
                
                //flag it as "read"
                //imap_setflag_full($mbox, $mailid, "\\Flagged");
                continue;
            }

            // get marketplace username
            preg_match('# from (.*)$#', $mail->subject, $hits);
            $username = trim($hits[1]);

            if(!in_array($username, $this->ignore_comments_from_users)){
                //get the message
                preg_match('#'.$username.' has (not )?purchased the item.#', $mail->message, $hits);
                $message = trim(substr($mail->message,0, strpos($mail->message, $hits[0])));
                $purchased = !isset($hits[1]);

                //do codeblocks (pre and code tags)
                preg_match_all('#<(pre|code)>(.*?)<\/(pre|code)>#si', $message, $codeblocks);
                foreach($codeblocks[0] as $i => $codeblock){
                    $message = str_replace($codeblock, '<'.$codeblocks[1][$i].'>'.htmlspecialchars($codeblocks[2][$i]).'</'.$codeblocks[1][$i].'>', $message);
                }
               
                if($this->h_html){
                    $message = nl2br($message);
                }

                $subject = 'New Comment for '.preg_replace('#^.*"([^"]+)".*$#', '"$1"', $mail->subject).' ['.$base_comment_id.'|'.$item_id.']';

                $note = '<strong>User:</strong> <a href="'.$marketplace.'/user/'.$username.'">'.$username.'</a><br>';
                $note .= '<strong>purchased:</strong> '.($purchased ? 'YES' : 'NO').'<br>';
                $note .= '<a href="'.$replylink.'">view comment</a>';

                $message = $this->convert_special_chars($message);

                echo '<textarea style="width:400px;height:200px;">'.print_r(($message), true).'</textarea>';

                $fromaddress = 'user_'.$username.'@'.ENVATO_CATCH_ALL_DOMAIN;

                //return;

               //send to helpscout
               $flag = $this->to_helpscout($this->h_comment_inbox, $username, $fromaddress, $message, $subject, $note);
               
           }
            
            //flag message
            if($flag) imap_setflag_full($mbox, $mailid, "\\Flagged");

        }

        imap_expunge($mbox);
        imap_close($mbox);

    }


    public function reply_comments(){
 
        $hostname = "{" . $this->catchall_server . ":" .$this->catchall_port . "/imap".($this->catchall_ssl ? "/ssl" : '')."/novalidate-cert}";

        $mbox = @imap_open($hostname."INBOX", $this->catchall_user, $this->catchall_password);

        if(!$mbox) die("Couldn't connect to host!"); 

        $this->log('Check for Comments to reply');
        
        $search = 'SUBJECT "New comment " SINCE "'.date('j F Y', strtotime('- 10 day')).'"';

        $m = imap_search($mbox, $search);
        
        $count = $m ? count($m) : 0;

        $to_deploy = array();
        
        $this->log($count.' comments found!');

        if(!$count) return;

        foreach($m as $mailid){

            $mail = $this->get_message($mbox, $mailid);

            $message = $mail->message;
        
            //sanitizes
            $message = str_replace(array("\r\n", "\r"), "\n", $message);
            $message = trim(preg_replace('/(?:(?:\r\n|\r|\n)\s*){2}/s', "==DOUBLELINEBREAK==", $message));
            $message = trim(preg_replace('/(?:(?:\r\n|\r|\n)\s*){1}/s', " ", $message));
            $message = str_replace("==DOUBLELINEBREAK==", "\n\n", $message);
            
            //get last response
            preg_match('/##(.*?)( replied)?, on(.*?):$/m', $message, $hits);
            $message = trim(substr($message, strpos($message, $hits[0])+strlen($hits[0])));
            $message = trim(substr($message,0, strpos($message, '-----------------------------------------------------------')));

            //keep only allowed tags
            $message = strip_tags($message, '<img><a><b><strong><em><i><u><ul><ol><li><br><code><pre><blockquote>');

            //we have links (index)
            if(preg_match('/Links: ------/', $message, $hits)){
                
                $message = trim(substr($message, 0, strpos($message, $hits[0])));
                //remove the [X] in the message
                $message = preg_replace('#(https?.*?)(\n)?\[\d+\]#ms', '$1', $message);

            }

            //we have attachments
            if(!empty($mail->attachments)){
                 //remove the attachments from the message
                 $message = trim(substr($message,0, strrpos($message, 'Attached:'))).'<br>';

                 foreach($mail->attachments as $attachment){
                    if($link = $this->imgur_upload_base64image($attachment)){
                        $imagepath .= "\n".'<br><a href="'.$link.'"><img src="'.str_replace('http://', '//', $link).'" /></a>';
                    }else{

                    }

                    $message .= $imagepath;
                }

            }


            //get item and comment id
            preg_match('#\[(\d+)\|(\d+)\]$#', $mail->subject, $hits);
            
            $item_id = intval($hits[2]);
            $comment_id = intval($hits[1]);

            $message = (htmlspecialchars_decode($message));

            echo '<textarea style="width:400px;height:200px;">'.print_r($message, true).'</textarea>';
            
            //collect the message and deploy it in a separate batch prevent double posting in some cases
            $to_deploy[] = (object) array(
                'message_id' => $mailid,
                'item_id' => $item_id,
                'comment_id' => $comment_id,
                'message' => $message,
            );


            $this->log($message);

        }

        //return;

        foreach($to_deploy as $message){
            
           if($comment_id = $this->scrapper()->post_comment($message->item_id, $message->comment_id, $message->message)){

                $this->log('Comment posted!');
                
                if($this->comment_signature){
                    
                    //update the comment with the signature
                    $message->message .= $this->comment_signature;
                    $this->scrapper()->update_comment($comment_id, $message->message);
                    
                    $this->log('Signature posted!');

                }


                imap_delete($mbox, $message->message_id);
            
            }else{  

                $error = "Not able to post comment:\n\n=========\n\n{$message->message}\n=========\n\nComment ID: {$message->comment_id}\nItem ID: {$message->item_id}";
                $this->error(__FUNCTION__, $error);
                $this->log('Not able to post comment!');
                //imap_delete($mbox, $message->message_id);
                break;
            }

        }


        imap_expunge($mbox);
            
        imap_close($mbox);

   }

    public function delete_messages() {
    
        $hostname = "{" . $this->server . ":" .$this->port . "/imap".($this->ssl ? "/ssl" : '')."/novalidate-cert}";

        $mbox = @imap_open($hostname."INBOX", $this->user, $this->password);

        if(!$mbox) die("Couldn't connect to host!"); 

        $this->log('Check for Messages to delete');
        
        $search = 'SUBJECT "'.$this->messagesubject.'" BEFORE "'.date('j F Y', strtotime('- '.$this->keep_messages)).'" TO "'.$this->user.'" FROM "do-not-reply@market.envato.com" FLAGGED';
       
  
        $m = imap_search($mbox, $search);
        
        $count = $m ? count($m) : 0;
        
        $this->log($count.' total messages found!');
        
        if(!empty($m)){

            foreach($m as $mailid){
                $header = imap_headerinfo($mbox, $mailid, 0);
                
                $timepased = time()-strtotime($header->date);
                
                //only messages at least X hours old
                if($timepased < 3600*$this->keep_messages) continue;
                    
                if(imap_delete($mbox, $mailid)){
                   $this->log('Message "'.$header->subject.'" deleted');
               }
                
            }
        }
            
        imap_expunge($mbox);
        imap_close($mbox);
        return;
        
    }


    public function delete_comments() {
    
        $hostname = "{" . $this->comment_server . ":" .$this->comment_port . "/imap".($this->comment_ssl ? "/ssl" : '')."/novalidate-cert}";

        $mbox = @imap_open($hostname."INBOX", $this->comment_user, $this->comment_password);

        if(!$mbox) die("Couldn't connect to host!"); 

        $this->log('Check for Comments to delete!');

        $search = 'SUBJECT "'.$this->commentsubject.'" BEFORE "'.date('j F Y', strtotime('- '.$this->keep_messages)).'" TO "'.$this->comment_user.'" FROM "do-not-reply@market.envato.com" FLAGGED';

        $m = imap_search($mbox, $search);
        
        $count = $m ? count($m) : 0;
        
        $this->log($count.' comments found!');

        if(!$count) return;

        if(!empty($m)){

            foreach($m as $mailid){
                $header = imap_headerinfo($mbox, $mailid, 0);
                
                $timepased = time()-strtotime($header->date);
                    
                if(imap_delete($mbox, $mailid)){
                   $this->log('Message "'.$header->subject.'" deleted');
               }
                
            }
        }
            
        imap_expunge($mbox);
        imap_close($mbox);
        return;
        
    }

    private function scrapper(){
        require_once 'classes/class.envato_scraper.php';

        //login if not already logged in
        if(is_null($this->loggedin)){
            $this->envato_scrapper = new envato_scraper();
            $this->loggedin = $this->envato_scrapper->do_login($this->envato_username, $this->envato_password);
        }

        return $this->envato_scrapper;
    }

    private function log($text){

        echo '<pre>'.print_r($text, true).'</pre>';

    }

    private function error($func, $message){

        $this->mail('Error on Helpscout to Envato in method '.$func, $message);
   
   }

    private function mail($subject, $message){

        if(!empty($this->notification_mail))
            @mail($this->notification_mail, $subject, $message);

    }
    private function get_message($mbox, $mailid){
       
        $mail = new StdClass;

        $structure = imap_fetchstructure($mbox, $mailid);
        $header = imap_headerinfo($mbox, $mailid, 0);

        $mail->subject = $header->subject;
        $mail->date = isset($header->MailDate) ? $header->MailDate : $header->date;
        $mail->timestamp = strtotime($header->date);
        $mail->to = $header->to[0];
        $mail->sender = isset($header->sender) ? $header->sender[0]->personal : $header->from[0]->personal;
        $mail->from = $header->from[0]->mailbox.'@'.$header->from[0]->host;
        $mail->reply_to = isset($header->reply_to) ? $header->reply_to[0]->mailbox.'@'.$header->reply_to[0]->host : $mail->from;
        $mail->attachments = array();

        $msg = @imap_fetchbody($mbox, $mailid, 1);
        $mail->org_message = $msg;
        $msg = @imap_qprint($msg);
        $msg = utf8_decode($msg);

        $mail->message = trim($msg);
        
        $msg = imap_fetchbody($mbox, $mailid, 1.2);
        if(empty($msg)){
            $msg = imap_fetchbody($mbox, $mailid, 2);
        }else{
            for($j = 2; $j < 99; $j++){
                $data = imap_fetchbody($mbox, $mailid, $j);
                if(empty($data)) break;
                $mail->attachments[] = $data;
            }
        }
        $mail->org_html_message = $msg;
        $msg = @imap_qprint($msg);
        $msg = utf8_decode($msg);

        $mail->html_message = trim($msg);
        return $mail;

    }

    private function get_base_comment_id($replylink, $comment_id) {
        

        $ch = curl_init($replylink);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $page = curl_exec($ch);
        
        curl_close($ch);
       
        $dom = new DOMDocument();
        $dom->strictErrorChecking = false;
        @$dom->loadHTML($page);
        $xpath = new DOMXPath($dom);
        $comments = $xpath->query('//div[@class="js-discussion comment__container-redesign"]');

        //all comments on this page
        foreach($comments as $comment){
            $commenthtml = $dom->saveXML($comment);
            //wrong comment!
            if(!preg_match('#<div class="comment__info" id="comment_'.$comment_id.'"#', $commenthtml)) continue;
            
            //get the first comment ID
            preg_match('#<div class="comment__info" id="comment_(\d+)">#', $commenthtml, $hits);
            $comment_id = intval(trim($hits[1]));
            return $comment_id;
           
        }

        return $comment_id;
       
    }

    private function to_helpscout($mailboxid, $username, $email, $message, $subject, $notetext) {

        try{

            $api = ApiClient::getInstance();
            //$api->setDebug($this->debug);
            $api->setKey($this->h_apikey);
            
            $mailbox = $api->getMailbox($mailboxid);
            $mailboxRef = $api->getMailboxProxy($mailboxid);
            
            $user = $api->getUsersForMailbox($mailboxid);
            $user = $user->getItems();
            $user = $user[0];
            $userid = $user->getID();
            $userRef = $api->getUserRefProxy($userid);
             
            $customers = $api->searchCustomersByEmail($email);

  
            //customer exists
            if($customers->getCount()){
            
                $this->log('OLD CUSTOMER');
                $items = $customers->getItems();
                $customer = $items[0];
                
            }else{
                
                $this->log('CREATE NEW CUSTOMER');

                $customer = new \HelpScout\model\Customer();
                $customer->setFirstName($username);
                
                $entry = new \HelpScout\model\customer\EmailEntry();
                $entry->setValue($email);
                $customer->addEmail($entry);
                            
                $api->createCustomer($customer);
            }

            $costumerid = $customer->getID();
            $customerRef = $api->getCustomerRefProxy($costumerid, $email);
            
            $createdBy = new \HelpScout\model\ref\PersonRef();
            $createdBy->setId($costumerid);
            $createdBy->setFirstName($username);
            $createdBy->setEmail($email);
            $createdBy->setType("customer");
            
            
            $conversations = $api->getConversationsForCustomerByMailbox($mailboxid, $costumerid);

            $is_new = true;
            
            if($conversations->getCount()){
                $items = $conversations->getItems();
                $conversation = $items[0];
                if($conversation->getSubject() == $subject){
                    $is_new = false;
                }else{
                    $is_new = true;
                }
            }

            $message = utf8_encode($message);
            $message = mb_convert_encoding($message, "UTF-8");

            //conversation exists
            if(!$is_new){
                
                $this->log('OLD CONVERSATION');
            
                $items = $conversations->getItems();
                $conversation = $items[0];
                $conversationid = $conversation->getID();
                $conversation = $api->getConversation($conversationid);
               
                $msg = new \HelpScout\model\thread\Customer();
                
                $msg->setBody(($message));
                $msg->setType('customer');
                $msg->setCreatedBy($createdBy);
                
                    
            }else{
            
                $this->log('CREATE NEW CONVERSATION');
                
                $conversation = new \HelpScout\model\Conversation();
                
                $conversation->setMailbox($mailboxRef);
                $conversation->setCreatedBy($customerRef);
                $conversation->setSubject($subject);
                $conversation->setCustomer($customerRef);
                
                $conversation->setType('customer');
                $conversation->setStatus('active');

                $note = new \HelpScout\model\thread\Note();
                $note->setBody($notetext);
                //$note->setType('note');
                $note->setCreatedBy($userRef);
            
                $msg = new \HelpScout\model\thread\Customer();
                $msg->setBody($message);
                $msg->setType('customer');
                $msg->setCreatedBy($createdBy);

                $conversation->addLineItem($note);
                $api->createConversation($conversation, true);
                $conversationid = $conversation->getID();
                
            }
        
            $api->createThread($conversationid, $msg);

            return true;

        }catch (Exception $e){

            $error = "Not able to use API:\n\n".print_r($e, true);
            $this->error(__FUNCTION__, $error);
            $this->log('Not able to send entry to HelpScout comment!');

            return false;
        }
        
      
        
    }

    private function convert_special_chars($string){
        $chars = array('À'=>'&Agrave;', 'à'=>'&agrave;', 'Á'=>'&Aacute;', 'á'=>'&aacute;', 'Â'=>'&Acirc;', 'â'=>'&acirc;', 'Ã'=>'&Atilde;', 'ã'=>'&atilde;', 'Ä'=>'&Auml;', 'ä'=>'&auml;', 'Å'=>'&Aring;', 'å'=>'&aring;', 'Æ'=>'&AElig;', 'æ'=>'&aelig;', 'Ç'=>'&Ccedil;', 'ç'=>'&ccedil;', 'Ð'=>'&ETH;', 'ð'=>'&eth;', 'È'=>'&Egrave;', 'è'=>'&egrave;', 'É'=>'&Eacute;', 'é'=>'&eacute;', 'Ê'=>'&Ecirc;', 'ê'=>'&ecirc;', 'Ë'=>'&Euml;', 'ë'=>'&euml;', 'Ì'=>'&Igrave;', 'ì'=>'&igrave;', 'Í'=>'&Iacute;', 'í'=>'&iacute;', 'Î'=>'&Icirc;', 'î'=>'&icirc;', 'Ï'=>'&Iuml;', 'ï'=>'&iuml;', 'Ñ'=>'&Ntilde;', 'ñ'=>'&ntilde;', 'Ò'=>'&Ograve;', 'ò'=>'&ograve;', 'Ó'=>'&Oacute;', 'ó'=>'&oacute;', 'Ô'=>'&Ocirc;', 'ô'=>'&ocirc;', 'Õ'=>'&Otilde;', 'õ'=>'&otilde;', 'Ö'=>'&Ouml;', 'ö'=>'&ouml;', 'Ø'=>'&Oslash;', 'ø'=>'&oslash;', 'Œ'=>'&OElig;', 'œ'=>'&oelig;', 'ß'=>'&szlig;', 'Þ'=>'&THORN;', 'þ'=>'&thorn;', 'Ù'=>'&Ugrave;', 'ù'=>'&ugrave;', 'Ú'=>'&Uacute;', 'ú'=>'&uacute;', 'Û'=>'&Ucirc;', 'û'=>'&ucirc;', 'Ü'=>'&Uuml;', 'ü'=>'&uuml;', 'Ý'=>'&Yacute;', 'ý'=>'&yacute;', 'Ÿ'=>'&Yuml;', 'ÿ'=>'&yuml;');

        $string = utf8_encode($string);
        
        foreach($chars as $entity => $sign){
            $string = str_replace($entity, $sign, $string);
        }

        return $string;

    }


    private function imgur_upload_base64image($base64image){

        if(!$this->imgur_api_id) return false;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.imgur.com/3/image.json');
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'Authorization: Client-ID ' . $this->imgur_api_id));
        curl_setopt($ch, CURLOPT_POSTFIELDS, array( 'image' => $base64image));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        $response = curl_exec($ch);
        $response = json_decode($response);

        if($response->status == 200){
            return $response->data->link;
        }

        return false;

    }


}