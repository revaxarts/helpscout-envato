<?php

if(!file_exists('config.php')) die('No config.php found!');

require 'config.php';
require 'classes/class.helpscout.envato.php';

$hse = new helpscout_envato();

//output some info, remove or set to false to prevent output
$hse->debug(true);

//how long keep original messages (default 1 day)
$hse->keep_messages('1 days');

//ignore comments from certain users (accepts string or array)
$hse->ignore_comments('revaxarts');

//use a signature below each comment - the signature will not be in the mail notification of the commenter
$hse->comment_signature("\n<br>__<br>\n".'<strong><sup><sup><a href="http://rxa.li/support?utm_source=Item+Comment">HELPDESK</a> | <a href="http://rxa.li/newsletter?utm_source=Item+Comment">NEWSLETTER</a> | <a href="https://twitter.com/revaxarts">TWITTER</a></sup></sup></strong>');


//check inbox for profile messages
$hse->check_inbox();

//check inbox for comments
$hse->check_comments();

//reply comments sent from HelpScout to your catch all address
$hse->reply_comments();

//remove certain messages
$hse->delete_messages();
$hse->delete_comments();

