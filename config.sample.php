<?php

//Envato login info for the envato scrapper
define('ENVATO_USERNAME', 'your-envato-username');
define('ENVATO_PASSWORD', 'your-envato-password');

//send errors to this address (uses mail function)
define('NOTIFICAITON_MAIL', '');


//IMAP server settings for profile messages
define('ENVATO_IMAP_SERVER', '');
define('ENVATO_IMAP_PORT', 25);
define('ENVATO_IMAP_USER', '');
define('ENVATO_IMAP_PASSWORD', '');
define('ENVATO_IMAP_SSL', false);

//IMAP server settings for comments
define('ENVATO_COMMENT_IMAP_SERVER', '');
define('ENVATO_COMMENT_IMAP_PORT', 25);
define('ENVATO_COMMENT_IMAP_USER', '');
define('ENVATO_COMMENT_IMAP_PASSWORD', '');
define('ENVATO_COMMENT_IMAP_SSL', false);

//catch all domain - comments get sent to user_username@[ENVATO_CATCH_ALL_DOMAIN]
define('ENVATO_CATCH_ALL_DOMAIN', '');

//IMAP server settings for reply comments
define('ENVATO_CATCH_ALL_IMAP_SERVER', '');
define('ENVATO_CATCH_ALL_IMAP_PORT', 25);
define('ENVATO_CATCH_ALL_IMAP_USER', '');
define('ENVATO_CATCH_ALL_IMAP_PASSWORD', '');
define('ENVATO_CATCH_ALL_IMAP_SSL', false);

//HELPSCOUT settings
define('HELPSCOUT_APIKEY', '');

//dedicate inbox for profile messages
define('HELPSCOUT_ENVATO_INBOX', 0);
//dedicate inbox for comments
define('HELPSCOUT_ENVATO_COMMENT_INBOX', 0);

//use html in HelpScout (fancy html)
define('HELPSCOUT_HTML_MAIL', true);

//OPTIONAL - IMGUR API KEY to upload images for comments to imgur.com
define('IMGUR_API_ID', '');