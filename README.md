# README #

This class helps you to manage all support requests via [HelpScout.net](http://helpscout.net) including item comments.

You can reply to comments and messages via your favorite email client

## What you need ##

* Envato account
* HelpScout Account with paid subscription (API access)
* recommended: dedicate HelpScout mailbox for comments
* catch-all email address
* IMAP support email server


## First step ##

Before you start upload the files to your server and copy the `config.sample.php` to `config.php` in the root folder

### Messages from your Profile page ###

The easy part is to handle messages sent from your profile page (http://themeforest.net/user/yourusername). Since Envato doesn't allow to define a dedicate email address to this messages you have to use your default marketplace email address.

Insert the information of your email server in the config.php file:
```
#!php

define('ENVATO_IMAP_SERVER', '');
define('ENVATO_IMAP_PORT', 25);
define('ENVATO_IMAP_USER', '');
define('ENVATO_IMAP_PASSWORD', '');
define('ENVATO_IMAP_SSL', false);
```

That's all if you just like to forward profile messages to HelpScout. You will still find them in your inbox but as soon the script progresses they will get flagged and deleted after a defined time.


### Working with Comments ###
Because the Envato API doesn't offer any interface for comments we have to use dtbalker's Envato Scrapper class (included) to post comments.

First you need a dedicate email address with [Catch-all](http://en.wikipedia.org/wiki/Catch-all) capability. So that all emails to @yourdomain.com will end in a single inbox